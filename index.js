module.exports = {
  // register plugins
  plugins: [
    require('esformatter-braces'),
    require('esformatter-curly'),
    require('esformatter-dot-notation'),
    require('esformatter-literal-notation'),
    require('esformatter-parseint'),
    require('esformatter-remove-object-spaces')
  ],

  indent: {
    ReturnStatement: '>=1'
  },

  whiteSpace: {
    after: {
      ObjectPatternOpeningBrace: 1,
      ObjectExpressionOpeningBrace: 1
    },
    before: {
      ObjectPatternClosingBrace: 1,
      ObjectExpressionClosingBrace: 1
    }
  }
};